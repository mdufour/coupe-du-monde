package exception;

/**
 * A class for the exceptions of the class Date
 */
public class DateException extends Exception {

	public static String messageErreur ;
	
	public DateException (String message) {
		
		super (message) ;
		messageErreur = message ;	
	}
}
