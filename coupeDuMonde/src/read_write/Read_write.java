package read_write;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Read_write {

	/**
	 * read the file in param and return an object of that reading
	 * @param  parFile : the file we want to read
	 * @return the read object
	 */
	public static Object reading (File parFile) {
		
		ObjectInputStream flow ;
	    Object readObject = null;
	    
		// Open the file
		try {
			flow = new ObjectInputStream(new FileInputStream (parFile));
			readObject = (Object)flow.readObject ();
			flow.close ();
	    }  // try
		
	    catch (ClassNotFoundException parException)	{
	    	System.err.println (parException.toString ());
			System.exit (1);
		} //catch

		catch (IOException parException) {
			System.err.println ("Erreur lecture du fichier " + parException.toString ());
			System.exit (1);
		} //catch
		
		return readObject ;   
	}  

	/**
	 * write the object in param in the file in param (erase) 
	 * @param parFile: the file where we want to write
	 * @param parObject: the object we want to write
	 * 
	**/
	public static void writing (File parFile, Object parObject) {
		
		ObjectOutputStream flow = null;
		
		// Open the file
		try {
			flow = new ObjectOutputStream (new FileOutputStream (parFile));
			flow.writeObject (parObject);
			flow.flush ();  
			flow.close ();
		} // try
		
		catch (IOException parException) {
			System.err.println ("Probleme a l'ecriture\n" + parException.toString ());
			System.exit (1);
		} // catch
	}
}
