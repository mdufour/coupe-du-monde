package data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.TreeSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class MatchData implements Comparable <MatchData> {

	/**
	 * date of the match
	 */
	String date;
	
	/**
	 * the number of the match
	 */
	int number;
	
	/**
	 * name of the first team
	 */
	String teamA;
	
	/**
	 * name of the second team
	 */
	String teamB ;
	
	/**
	 * score of the first team
	 */
	int scoreA;
	
	/**
	 * score of the second team
	 */
	int scoreB;

	/**
	 * to check if a field is modified
	 */
	int updated;
	
	// get	
	public String getDate() {
		
		return date;
	}
	
	public String getTeamA() {
		
		return teamA;
	}
	
	public String getTeamB() {
		
		return teamB;
	}
	
	public int getScoreA() {
		
		return scoreA;
	}

	public int getScoreB() {
	
		return scoreB;
	}
	
	public int getUpdated () {
		
		return updated;
	}
	
	// Constructor
	public MatchData (int parUpdated, int parNumber, String parDate, String parTeamA, String parTeamB, int parScoreA, int parScoreB)
	{
		updated = parUpdated;
		number = parNumber;
		date = parDate;
		teamA = parTeamA;
		teamB = parTeamB;
		scoreA = parScoreA;
		scoreB = parScoreB ;
	}
    
    /**
     * @param the object MatchData you need to compare
     * @return the result of the comparison of the 2 dates this.date and match.date
     */
	public int compareTo (MatchData match) { 
		
		Integer a = new Integer(this.number);
		Integer b = new Integer(match.number);
		return a.compareTo(b);
	}
		
	/**
	 * read the XML file that contain the matches
	 * @param the path of the XML file (String)
	 * @return a TreeSet <MatchData>
	**/
	public static TreeSet <MatchData> readingXMLFile (String filePath) {
		
		TreeSet <MatchData> matches = new TreeSet <MatchData> ();
		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null; 
	    
		try {
			builder = builderFactory.newDocumentBuilder();
			Document matchDocument = builder.parse(new FileInputStream(filePath));
			NodeList matchNodeList = matchDocument.getElementsByTagName("match");
		   
			for (int i = 0 ; i < matchNodeList.getLength() ; i++)	   
			{
				Element current = (Element) matchNodeList.item(i);
				int number = Integer.parseInt(current.getAttribute("number"));
				String date = current.getAttribute("date");
				String teamA = current.getAttribute("teamA");
				String teamB = current.getAttribute("teamB");
				int scoreA = Integer.parseInt(current.getAttribute("scoreA"));
				int scoreB = Integer.parseInt(current.getAttribute("scoreB"));	
				int updated = Integer.parseInt(current.getAttribute("updated"));
		       
				MatchData match = new MatchData (updated, number, date, teamA, teamB, scoreA, scoreB);				
				matches.add (match); 
			}
		} //try
	    
		catch (SAXException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		} 
		catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		
		return matches;
	}
	
	/**
	 * write in the XML file that contain the matches
	 * @param the path of the XML file (String)
	 * @return a TreeSet <MatchData>
	**/
	public static void writingXMLFile (String filePath, String date, String teamA, String teamB, String scoreA, String scoreB) {
		
		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null; 
	    
		try {
			builder = builderFactory.newDocumentBuilder();
			Document matchDocument = builder.parse(new FileInputStream(filePath));
			NodeList matchNodeList = matchDocument.getElementsByTagName("match");
		   
			for (int i = 0 ; i < matchNodeList.getLength() ; i++)	   
			{
				Element current = (Element) matchNodeList.item(i);
				
				if (current.getAttribute("date").compareTo(date) == 0 && current.getAttribute("teamA").compareTo(teamA) == 0 && current.getAttribute("teamB").compareTo(teamB) == 0) {
					current.setAttribute("scoreA", scoreA);
					current.setAttribute("scoreB", scoreB);
					current.setAttribute("updated", "1");
					
					TransformerFactory tfact =  TransformerFactory.newInstance();
			        Transformer transformer =  tfact.newTransformer();
			        DOMSource source = new DOMSource(matchDocument);
			        File fichier= new File(filePath);
			        FileWriter fw = new FileWriter(fichier);
			 
			        StreamResult result = new StreamResult(fw);
			        transformer.transform(source, result);
				}
			}
		} //try
	    
		catch (SAXException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		} 
		catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
