package data;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.TreeSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class TeamData implements Comparable <TeamData> {

	/**
	 * name of the country
	 */
	String country;
	
	/**
	 * path of the flag of the team
	 */
	String flag;
	
	/**
	 * number of the pot
	 */
	int pot ;
	
	/**
	 * letter of the pool
	 */
	char pool;
	
	// get	
	public String getCountry() {
		
		return country;
	}
	
	public String getFlag() {
		
		return flag;
	}
	
	public char getPool() {
		
		return pool;
	}
	
	// Constructor
	public TeamData (String parCountry, String parFlag, int parPot, char parPool)
	{
		country = parCountry;
		flag = parFlag;
		pot = parPot;
		pool = parPool ;
	}
	
	/**
	 * @return a String with the country, the path of the flag, the pot and the pool
	 */
    public String toString ()
    {
    	return country + " " + flag + " chapeau : " + pot
    			+ " groupe : " + pool + "\n";
    } 
    
    /**
     * @param the object Team you need to compare
     * @return the result of the comparison of the 2 strings this.country and team.country
     */
	public int compareTo (TeamData team) { 
		return this.country.compareTo(team.country);
	}
		
	/**
	 * read the XML file that contain the teams
	 * @param the path of the XML file (String)
	 * @return a TreeSet <Team>
	**/
	public static TreeSet <TeamData> readingXMLFile (String filePath) {
		
		TreeSet <TeamData> teams = new TreeSet <TeamData> ();
		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null; 
	    
		try {
			builder = builderFactory.newDocumentBuilder();
			Document teamDocument = builder.parse(new FileInputStream(filePath));
			NodeList countryNodeList = teamDocument.getElementsByTagName("equipe");
		   
			for (int i=0 ; i < countryNodeList.getLength() ; i++)	   
			{
				Element current =  (Element) countryNodeList.item(i);
				String country = current.getAttribute("pays");
				String flag = current.getAttribute("drapeau");
				int pot = Integer.parseInt(current.getAttribute("chapeau"));
				char pool = current.getAttribute("groupe").charAt(0); 
		       
				TeamData team = new TeamData (country, flag, pot, pool); 
				teams.add (team); 
			}
		} //try
	    
		catch (SAXException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		} 
		catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		
		return teams;
	}
	
	public static String [] teams (String filePath) {
		
		String [] allTeams = new String [32];
		TeamData teamIterator;
		Iterator iterator = readingXMLFile (filePath).iterator ();
		
		for (int i = 0; iterator.hasNext(); i++) {
			teamIterator = (TeamData) iterator.next();
			allTeams [i] = teamIterator.country;
		}
		
		return allTeams;
	}
}
