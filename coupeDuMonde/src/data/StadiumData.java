package data;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.TreeSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class StadiumData implements Comparable <StadiumData> {
	
	/**
	 * name of the stadium
	 */
	String name;
	
	/**
	 * city where the stadium is
	 */
	String city;
	
	// Constructor
	public StadiumData (String parName, String parCity)
	{
		name = parName;
		city = parCity;
	}
	
	/**
	 * @return a String with the name and the city
	 */
    public String toString ()
    {
    	return "Stadium : " + name + ", " + city + "/n";
    } 
    
    /**
     * @param the object Stadium you need to compare
     * @return the result of the comparison of the 2 strings this.country and team.country
     */
	public int compareTo (StadiumData stadium) { 
		return this.name.compareTo(stadium.name);
	}
	
	/**
	 * read the XML file that contain the stadiums
	 * @param the path of the XML file (String)
	 * @return a TreeSet <Stadium>
	**/
	public static TreeSet<StadiumData> readingXMLFile (String filePath) {
		
		TreeSet <StadiumData> stadiums = new TreeSet <StadiumData> ();
		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null; 
	    
		try {
			builder = builderFactory.newDocumentBuilder();
			Document stadiumsDocument = builder.parse (new FileInputStream(filePath));
			NodeList countryNodeList = stadiumsDocument.getElementsByTagName ("stadium");
		   
			for (int i=0 ; i < countryNodeList.getLength() ; i++)	   
			{
				Element current =  (Element) countryNodeList.item(i);
				String city = current.getAttribute("city");
				String name = current.getAttribute("name");
				
				StadiumData stadium = new StadiumData (name, city); 
				stadiums.add (stadium); 
			}
		} //try
	    
		catch (SAXException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		} 
		catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		
		return stadiums;
	}
	
	public static String [] stadiums (String filePath) {
		
		String [] allStadiums = new String [12];
		StadiumData stadiumIterator;
		Iterator <StadiumData> iterator = readingXMLFile (filePath).iterator ();
		
		for (int i = 0; iterator.hasNext(); i++) {
			stadiumIterator = (StadiumData) iterator.next();
			allStadiums [i] = stadiumIterator.name;
		}
		
		return allStadiums;
	}
}
