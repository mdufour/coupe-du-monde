package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.TeamModel;
import view.TeamPanelEndOfPool;

/**
 * The controller of the TeamEndOfPoolPanel
 */
public class TeamControllerEndOfPool implements ActionListener {

	static TeamPanelEndOfPool panel;
	TeamModel model;
	
	public TeamControllerEndOfPool (TeamModel parModel) {

		model = parModel;
	}
	
	public void setView (TeamPanelEndOfPool parPanel) {
		
		panel = parPanel;
	}
	
	public void actionPerformed(ActionEvent e) {		
		
		if (e.getSource() == panel.getPrevious ()) {
			panel.setPanelsLayoutPrevious();
			model.setPreviousPool();
		}
		
		else if (e.getSource() == panel.getNext ()) {
			panel.setPanelsLayoutNext();
			model.setNextPool();
		}
	}

	public static void refresh() {
		
		panel.refresh ();
	}
}
