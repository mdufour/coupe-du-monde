package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.MatchPanel;
import model.MatchModel;

/**
 * The controller of the MatchPanel
 */
public class MatchController implements ActionListener {

	static MatchPanel panel;
	MatchModel model;
	
	public MatchController (MatchModel parModel) {

		model = parModel;
	}
	
	public void setView (MatchPanel parPanel) {
		
		panel = parPanel;
	}
	
	public void actionPerformed(ActionEvent e) {		
		
		if (e.getSource() == panel.getPrevious ()) {
			panel.setPanelsLayoutPrevious();
			model.setPreviousPool();
		}
		
		else if (e.getSource() == panel.getNext ()) {
			panel.setPanelsLayoutNext();
			model.setNextPool();
		}
	}

	public static void refresh() {
		
		panel.refresh ();
	}
}
