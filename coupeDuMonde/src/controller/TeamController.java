package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.TeamPanel;
import model.TeamModel;

/**
 * The controller of the TeamPanel
 */
public class TeamController implements ActionListener {

	TeamPanel panel;
	TeamModel model;
	
	public TeamController (TeamModel parModel) {

		model = parModel;
	}
	
	public void setView (TeamPanel parPanel) {
		
		panel = parPanel;
	}
	
	public void actionPerformed(ActionEvent e) {		
		
		if (e.getSource() == panel.getPrevious ()) {
			panel.setPanelsLayoutPrevious();
			model.setPreviousPool();
		}
		
		else if (e.getSource() == panel.getNext ()) {
			panel.setPanelsLayoutNext();
			model.setNextPool();
		}
	}
}
