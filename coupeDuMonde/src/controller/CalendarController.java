package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import data.MatchData;
import view.AddingPanel;
import view.CalendarPanel;
import view.DayJButton;
import model.CalendarModel;
import model.ScoreModel;

/**
 * The controller of the CalendarPanel
 */
public class CalendarController implements ActionListener {

	CalendarPanel panel;
	CalendarModel model;
	AddingPanel addingPanel;
	ScoreModel scoreModel;
	
	public CalendarController (CalendarModel parModel, ScoreModel parScoreModel, AddingPanel parAddingPanel) {

		model = parModel;
		addingPanel = parAddingPanel;
		scoreModel = parScoreModel;
	}
	
	public void setView (CalendarPanel parPanel) {
		
		panel = parPanel;
	}
	
	public void actionPerformed(ActionEvent e) {		
		
		if (e.getSource() == panel.getPrevious ()) {
			panel.setMonthsLayoutPrevious();
			model.setTextMonthPrevious(panel.getMonthName ());
		}
		
		else if (e.getSource() == panel.getNext ()) {
			panel.setMonthsLayoutNext();
			model.setTextMonthNext(panel.getMonthName ());
		}
		
		else if (e.getSource() == addingPanel.getAddButton()) {
			
			MatchData.writingXMLFile("data/matches_2014.xml", addingPanel.getDate().toString(5), addingPanel.getTeam1(), addingPanel.getTeam2(), addingPanel.getScore1(), addingPanel.getScore2());			
				
			scoreModel.scoreCalculation();
			addingPanel.refresh ();
			MatchController.refresh();
			TeamControllerEndOfPool.refresh();
		}
		
		else {
			model.setDate (((DayJButton) e.getSource ()).getDate ());
		}
	}
}
