package panel;

import model.MatchModel;
import view.MatchPanel;
import controller.MatchController;

/**
 * The class Match initialize all the components for the matchPanel 
 */
public class Match {

	MatchModel matchModel;
	MatchPanel matchPanel;
	static MatchController matchController;
	
	public Match () {
		
		matchModel = new MatchModel ();		
		matchController =  new MatchController (matchModel);
		matchPanel = new MatchPanel (matchModel);

		matchController.setView(matchPanel);
		matchPanel.getPrevious().addActionListener(matchController);
		matchPanel.getNext().addActionListener(matchController);
	}

	public MatchPanel getMatchView() {
		
		return matchPanel;
	}
}
