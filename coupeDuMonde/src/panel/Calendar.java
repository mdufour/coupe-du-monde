package panel;

import controller.CalendarController;
import view.AddingPanel;
import view.CalendarPanel;
import view.DayJButton;
import model.CalendarModel;
import model.ScoreModel;

/**
 * The class Calendar initialize all the components for the calendar and the addingPanel 
 */
public class Calendar {

	CalendarModel calendarModel;
	CalendarPanel calendarView;
	static CalendarController calendarController;
	AddingPanel addingPanel;
	ScoreModel scoreModel;
	
	public Calendar () {
		
		scoreModel = new ScoreModel ();
		scoreModel.scoreCalculation();
		calendarModel = new CalendarModel ();
		addingPanel = new AddingPanel (calendarModel);
		calendarController =  new CalendarController (calendarModel, scoreModel, addingPanel);
		calendarView = new CalendarPanel (calendarModel);

		calendarController.setView(calendarView);
		
		calendarView.getPrevious().addActionListener(calendarController);
		calendarView.getNext().addActionListener(calendarController);
		addingPanel.getAddButton().addActionListener(calendarController);
	}

	public CalendarPanel getCalendarView() {
		
		return calendarView;
	}

	public static void addActionListener(DayJButton day) {

		day.addActionListener(calendarController);
		
	}

	public AddingPanel getAddingPanel() {
		
		return addingPanel;
	}
	
	public ScoreModel getScoreModel() {
		
		return scoreModel;
	}	
}
