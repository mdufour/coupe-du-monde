package panel;

import model.ScoreModel;
import model.TeamModel;
import view.TeamPanelEndOfPool;
import controller.TeamControllerEndOfPool;

/**
 * The class TeamEndOfPool initialize all the components for the final panel of the teams 
 */
public class TeamEndOfPool {

	TeamPanelEndOfPool teamView;
	static TeamControllerEndOfPool teamController;
	TeamModel teamModel;
	
	public TeamEndOfPool (ScoreModel parScorePanel) {
			
		teamModel = new TeamModel ();
		teamController =  new TeamControllerEndOfPool (teamModel);
		teamView = new TeamPanelEndOfPool (teamModel, parScorePanel);

		teamController.setView(teamView);
		teamView.getPrevious().addActionListener(teamController);
		teamView.getNext().addActionListener(teamController);
	}

	public TeamPanelEndOfPool getTeamView() {
		
		return teamView;
	}
}
