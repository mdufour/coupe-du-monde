package panel;

import model.TeamModel;
import controller.TeamController;
import view.TeamPanel;

/**
 * The class Team initialize all the components for the teamPanel (with the pools) 
 */
public class Team {

	TeamModel teamModel;
	TeamPanel teamView;
	static TeamController teamController;
	
	public Team () {
		
		teamModel = new TeamModel ();		
		teamController =  new TeamController (teamModel);
		teamView = new TeamPanel (teamModel);

		teamController.setView(teamView);
		teamView.getPrevious().addActionListener(teamController);
		teamView.getNext().addActionListener(teamController);
	}

	public TeamPanel getTeamView() {
		
		return teamView;
	}
	
	public TeamModel getTeamModel() {
		
		return teamModel;
	}	
}
