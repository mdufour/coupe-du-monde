package model;

import java.util.HashMap;
import java.util.Iterator;

import data.MatchData;
import data.TeamData;

public class ScoreModel {

	Iterator <MatchData> matchesIterator; 
	Iterator <TeamData> teamsIterator;
	HashMap <String, Integer> results = new HashMap <String, Integer> ();
	MatchData match;
	TeamData team;
	int [] poolMatchesTable = new int [8];
	
	// get
	public HashMap <String, Integer> getResults () {
		
		return results;
	}
	
	// Constructor
	public ScoreModel () {
	
		for (int i = 0; i < poolMatchesTable.length; i++)
			poolMatchesTable [i] = 0;
	}
	
	// Calculation of the score of the teams
	public void scoreCalculation () {
		
		results = new HashMap <String, Integer> ();
		matchesIterator = MatchModel.getMatches().iterator();
		
		while (matchesIterator.hasNext()) {				
			match = matchesIterator.next();
			
			teamsIterator = TeamModel.getTeams().iterator();
			
			while (teamsIterator.hasNext ()) {					
				team = teamsIterator.next();
				
				if (results.containsKey(match.getTeamA()) != true) {
					
					results.put(match.getTeamA(), 0);
				}
				
				else if (results.containsKey(match.getTeamB()) != true) {
					
					results.put(match.getTeamB(), 0);
				}
				
				if (match.getTeamA().compareTo(team.getCountry()) == 0) {
					
					if (match.getUpdated() == 1) {
						
						results.put(match.getTeamA(), results.get(match.getTeamA()) + getPointsFromScore (match.getScoreA(), match.getScoreB()));
						results.put(match.getTeamB(), results.get(match.getTeamB()) + getPointsFromScore (match.getScoreB(), match.getScoreA()));
						poolMatchesTable [TeamModel.poolToInt(team.getPool())] ++;	
					}
				}
			}
		}
		
		for (int i = 0; i < poolMatchesTable.length; i++) {
			
			if (poolMatchesTable [i] >= 6) {
				System.out.println(results.toString());
			}
		}
	}
	
	public int getPointsFromScore (int scoreA, int scoreB) {
		
		int points;
		
		if (scoreA > scoreB) {
			points = 3;
		}
		
		else if (scoreA == scoreB) {
			points = 1;
		}
		
		else {
			points = 0;
		}
		
		return points;
	}
}
