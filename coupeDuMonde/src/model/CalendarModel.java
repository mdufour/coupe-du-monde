package model;

import java.util.Observable;
import data.Days_months;

public class CalendarModel extends Observable {

	private String monthName;
	private String [] monthsNames = Days_months.monthsNames;
	private Date date = new Date ();
	
	public void setTextMonthPrevious (String parMonthName) {

		monthName = parMonthName;
		
		for (int i = 0; i < monthsNames.length; i++) {
			
			if (monthName.matches(monthsNames [i]) && i > 0) {
				monthName = monthsNames [i - 1];
				i = monthsNames.length;
			}
			
			else if (monthName.matches(monthsNames [i]) && i == 0) {
				monthName = monthsNames [11];
				i = monthsNames.length;
			}
		}
		
		setChanged ();
		notifyObservers ();
	}
	
	public void setTextMonthNext (String parMonthName) {
		
		monthName = parMonthName;
		
		for (int i = 0; i < monthsNames.length; i++) {
			
			if (monthName.matches(monthsNames [i]) && i < monthsNames.length-1) {
				monthName = monthsNames [i + 1];
				i = monthsNames.length;
			}
			
			else if (monthName.matches(monthsNames [i]) && i == monthsNames.length-1) {
				monthName = monthsNames [0];
				i = monthsNames.length;
			}
		}

		setChanged ();
		notifyObservers ();
	}

	public String getMonthName() {
		
		return monthName;
	}
	
	public Date getDate () {
		
		return date;
	}
	
	public void setDate (Date parDate) {
		
		date = parDate;
		monthName = parDate.getMonthInLetter();
		setChanged ();
		notifyObservers ();
	}
}
