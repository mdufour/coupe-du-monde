package model;

import java.util.Iterator;
import java.util.Observable;
import java.util.TreeSet;

import data.MatchData;
import data.TeamData;

public class MatchModel extends Observable {
	
	/**
	 * A TreeSet with all the team of the competition
	 */
	TreeSet <TeamData> teams;
	
	/**
	 * A TreeSet with all the matches of the competition
	 */
	static TreeSet <MatchData> matches = MatchData.readingXMLFile ("data/matches_2014.xml");
	
	/**
	 * The iterator for the matches
	 */
	public static Iterator <MatchData> matchesIterator = matches.iterator();
	
	/**
	 * The iterator for the teams
	 */
	Iterator <TeamData> teamsIterator;
	
	/**
	 * A team
	 */
	TeamData team;
	
	/**
	 * letter of the pool
	 */
	private char pool = 'A';
	
	private static char [] poolNameTable = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'};
	
	// get
	public String getPoolString() {
		
		return "Groupe " + pool;
	}
	
	public static TreeSet<MatchData> getMatches () {
		
		matches = MatchData.readingXMLFile ("data/matches_2014.xml");
		return matches;
	}
	
	// set
	public void setNextPool() {		
		
		for (int i = 0; i < poolNameTable.length; i++) {

			if (pool == poolNameTable [i]) {
				pool =  poolNameTable [(i + 1) % poolNameTable.length];
				i = poolNameTable.length;
			}
		}	
		
		setChanged ();
		notifyObservers ();
	}
		
	public void setPreviousPool() {
			
		for (int i = 0; i < poolNameTable.length; i++) {

			if (poolNameTable [i] == pool) {
				pool =  poolNameTable [(i - 1 + poolNameTable.length) % poolNameTable.length];
				i = poolNameTable.length;
			}
		}
		
		setChanged ();
		notifyObservers ();
	}
	
	// Find the team with the country in param
	public TeamData findTeam (String parCountry) {
		
		teams = TeamData.readingXMLFile ("data/equipes_2014.xml");
		
		teamsIterator = teams.iterator();
		
		while (teamsIterator.hasNext()) {
			
			team = teamsIterator.next();
			
			if (team.getCountry().compareTo(parCountry) == 0)
				return team;					
		}
		
		return team;
	}
}
