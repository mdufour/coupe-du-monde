package model;

import java.util.Calendar;
import java.util.GregorianCalendar;
import exception.DateException;

public class Date
{
	// Variables
	private String dayInLetter ;
	private String monthInLetter ;
	private  int hours ;
	private  int minutes ;
	private  int day ;
	private  int month ;
	private  int year ;
	
	// Accessors
	public int getHours() {
		return hours;
	}
	
	public int getMinutes() {
		return minutes;
	}

	public int getDay ()
	{
		return day ;
	}
	
	public String getDayInLetter ()
	{
		return dayInLetter ;
	}
	
	public String getMonthInLetter ()
	{
		return monthInLetter ;
	}
	
	public int getMonth ()
	{
		return month ;
	}
	
	public int getYear ()
	{
		return year ;
	}	
	
	
	// Constructors
	public Date (int parDay, int parMonth, int parYear, int parMinutes, int parHours) throws DateException
	{
		if (parYear < 1582)
			throw new DateException ("L'annee doit etre superieur a 1582.") ;
		
		else if (parMonth < 1 || parMonth > 12)
			throw new DateException ("Le mois doit etre comprie entre 1 et 12.") ;
		
		else if (parDay < 1 || parDay > nbDays (parMonth, parYear))
			throw new DateException ("Le jour n'est pas valide pour ce mois.") ;
		
		else if (parHours < 0 || parHours > 23)
			throw new DateException ("L'heure n'est pas valide.") ;
		
		else if (parMinutes < 0 || parMinutes > 59)
			throw new DateException ("Les minutes ne sont pas valide.") ;
			
		else {
			minutes = parMinutes ;
			hours = parHours ;			
			day = parDay ;
			month = parMonth ;
			year = parYear ;
			dayInLetter = dayString (day, month, year) ;
			monthInLetter = monthString (month) ;
		}		
	}	
	
	public Date (int parDay, int parMonth, int parYear) throws DateException
	{
		if (parYear < 1582)
			throw new DateException ("L'annee doit etre superieur a 1582.") ;
		
		else if (parMonth < 1 || parMonth > 12)
			throw new DateException ("Le mois doit etre comprie entre 1 et 12.") ;
		
		else if (parDay < 1 || parDay > nbDays (parMonth, parYear))
			throw new DateException ("Le jour n'est pas valide pour ce mois.") ;		
			
		else {
			day = parDay ;
			month = parMonth ;
			year = parYear ;
			dayInLetter = dayString (day, month, year) ;
			monthInLetter = monthString (month) ;
		}		
	}
	
	public Date () 
	{
		GregorianCalendar today = new GregorianCalendar();
		year = today.get(Calendar.YEAR) ;
		month = today.get(Calendar.MONTH)+1 ;
		day = today.get(Calendar.DAY_OF_MONTH) ;	
		dayInLetter = dayString (day, month, year) ;
		monthInLetter = monthString (month) ;		
	}
	// End constructors
	
	/** 
	 * converts the day into letters 
	 * @param parDay : the day you want to convert
	 * @param parMonth : the month
	 * @param parYear : the year
	 * @return the day (String)
	 **/
	public static String dayString (int parDay, int parMonth, int parYear)
	{
		GregorianCalendar date = new GregorianCalendar(parYear, parMonth-1, parDay);
		
		switch (date.get(Calendar.DAY_OF_WEEK))
		{
			case 1 :
				return "Dimanche" ;
			case 2 :
				return "Lundi" ;
			case 3 :
				return "Mardi" ;
			case 4 :
				return "Mercredi" ;
			case 5 :
				return "Jeudi" ;
			case 6 :
				return "Vendredi" ;
			case 7 :
				return "Samedi" ;
			default :
				return "Lundi" ;
		}		
	}
	
	/** 
	 * converts the day into letters 
	 * @param parDay : the day you want to convert
	 * @param parMonth : the month
	 * @param parYear : the year
	 * @return the day (String)
	 **/
	public String dayString ()
	{
		GregorianCalendar date = new GregorianCalendar(this.year, this.month-1, this.day);
		
		switch (date.get(Calendar.DAY_OF_WEEK))
		{
			case 1 :
				return "Dimanche" ;
			case 2 :
				return "Lundi" ;
			case 3 :
				return "Mardi" ;
			case 4 :
				return "Mercredi" ;
			case 5 :
				return "Jeudi" ;
			case 6 :
				return "Vendredi" ;
			case 7 :
				return "Samedi" ;
			default :
				return "Lundi" ;
		}		
	}
	
	/** 
	 * converts the month into letters 
	 * @param parMonth : the month you want to convert 
	 * @return the month (String)
	 **/
	public static String monthString (int parMonth) {
		
		switch (parMonth) {
		
			case 1 :
				return "Janvier" ;				
			case 2 :
				return "F�vrier" ;								
			case 3 :
				return "Mars" ;					
			case 4 :
				return "Avril" ;				
			case 5 :
				return "Mai" ;				
			case 6 :
				return "Juin" ;				
			case 7 :
				return "Juillet" ;				
			case 8 :
				return "Ao�t" ;					
			case 9 :
				return "Septembre" ;					
			case 10 :
				return "Octobre" ;					
			case 11 :
				return "Novembre" ;				
			case 12 :
				return "D�cembre" ;
			default :
				return "Janvier" ;				
		}
	}
	
	/** 
	 * converts the date into a String
	 * @param type : the type of display you want 	 * 
	 * @return the date (String)
	 **/
	public String toString(int type) {
		
		switch (type) {
		
			// DD/MM/YYYY
			case 1 :
				return day + "/" + month + "/" + year ;
			
			// MM/DD/YYYY
			case 2 :
				return month + "/" + day + "/" + year ;
				
			// day DD month year	
			case 3 : 
				return dayInLetter + " " + day + " " + monthInLetter + " " + year ;
			
			// day DD month
			case 4 :
				return dayInLetter + " " + day + " " + monthInLetter ;
				
			// DD month year
			case 5 :
				return day + " " + monthInLetter + " " + year;
				
			default : 
				return day + "/" + month + "/" + year ;
		}
		
	}

	/** 
	 * nbDay returns the number of days in the month and the year in param 
	 * @param parMonth : the month
	 * @param parYear : the year
	 * @return the number of days in the month and the year in param (int)
	 **/
	public static int nbDays (int parMonth, int parYear) {
		
		switch (parMonth) {
		
			/* case 4, 6, 9, 11 */
			case 4:
			case 6:
			case 9:
			case 11:
				return 30 ;	
				
			/* case 1, 3, 5, 7, 8, 10, 12 */
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				return 31 ;
			case 2 : 
				if (leapYear (parYear) == false) 
					return 28 ;
				else 
					return 29 ;				
			default :
				return 0 ;				
		}
	}
	
	/** 
	 * tests if the year in param is a leap year 
	 * @param parYear : the year you want to test
	 * @return true if it's a leap year, else, return false
	 **/
	public static boolean leapYear (int parYear) {
		
		if (parYear%4 == 0 && parYear%100 != 0 || parYear%400 == 0)
			return true;
		else 
			return false;
	}
	
	/** 
	 * tests if this is before the date in param 
	 * @param date : a date to test if it's after this
	 * @return 1 if this is before date, -1 if this is after date, 0 if this and date are equals
	 **/
	public int before (Date date) {
		
		if (date.year < this.year)
			return -1 ;		
			
		else if (date.year > this.year)		
			return 1 ;
		
		
		else {
			
			if (date.month < this.month)
				return -1 ;
			
			else if (date.month > this.month)
				return 1 ;
				
			else {
				
				if (date.day < this.day)
					return -1 ;
			
				else if (date.day > this.day)
					return 1 ;
				else
					return 0 ;
			}	
		}
		
	} 
	
	/** 
	 * tests if this is before the date in param 
	 * @param date : a date to test if it's after this
	 * @return true if this is before date, false if this is after date
	 **/
	public boolean beforeBool (Date date) {
		
		if (date.year < this.year)
			return false ;		
			
		else if (date.year > this.year)		
			return true ;
		
		
		else {
			
			if (date.month < this.month)
				return false ;
			
			else if (date.month > this.month)
				return true ;
				
			else {
				
				if (date.day <= this.day)
					return false ;
			
				else if (date.day > this.day)
					return true ;
			}	
		}
		return false;
	} 
	
	/**
	 * tests if the date is the current date 
	 *  @return true if it's the current date, else, return false
	 **/
	public boolean isCurrentDate ()
	{
		Date today = new Date ();
		
		if (this.day == today.day && this.month == today.month && this.year == today.year)
			return true;		
		else
			return false;		
	}
	
	/**
	 *  tests if the date is a week-end
	 *  @param parDay : the day of the date
	 *  @param parMonth : the month of the date
	 *  @param parYear : the year of the date
	 *  @return true if it's a week-end, else, return false
	 **/
	public static boolean isWeekEnd (int parDay, int parMonth, int parYear)
	{
		GregorianCalendar date = new GregorianCalendar (parYear, parMonth - 1, parDay ) ;
		
		if (date.get(Calendar.DAY_OF_WEEK) == 1 || date.get(Calendar.DAY_OF_WEEK) == 7)
			return true ;
		else
			return false ;
	}	
	
	/**
	 *  moves to the next day 
	 *  @return the next day of the date parDate
	 **/
	public Date next ()
	{
		Date nextDay = null;
		
		if (this.day < nbDays (this.month, this.year)) {
			try {
				nextDay = new Date (this.day + 1, this.month, this.year);
			} 
			catch (DateException e) {
				e.printStackTrace();
			}
		}
		
		else if (this.month < 12) {
			try {
				nextDay = new Date (1, this.month + 1, this.year);
			} 
			catch (DateException e) {
				e.printStackTrace();
			}
		}
		
		else 
			try {
				nextDay = new Date (1, 1, this.year + 1);
			} 
			catch (DateException e) {
				e.printStackTrace();
			}
		
		return nextDay;
	}	
	
	/**
	 *  return the duration in months between two dates 
	 *  @return the next day of the date parDate
	 **/
	public static int durationMonth (Date startDate, Date endDate)
	{
		return ((endDate.year - startDate.year) * 12 + (endDate.month - startDate.month));
	}	
}