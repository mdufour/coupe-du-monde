package model;

import java.util.Observable;
import java.util.TreeSet;

import data.TeamData;

public class TeamModel extends Observable {

	/**
	 * A TreeSet with all the team of the competition
	 */
	static TreeSet <TeamData> teams = TeamData.readingXMLFile ("data/equipes_2014.xml");
		
	private char pool = 'A';
	
	private static char [] poolNameTable = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'};
	
	// get
	public String getPoolString() {
		
		return "Groupe " + pool;
	}
	
	public static TreeSet<TeamData> getTeams () {
		
		return teams;
	}
	
	// set
	public void setNextPool() {		
		
		for (int i = 0; i < poolNameTable.length; i++) {

			if (pool == poolNameTable [i]) {
				pool =  poolNameTable [(i + 1) % poolNameTable.length];
				i = poolNameTable.length;
			}
		}	
		
		setChanged ();
		notifyObservers ();
	}
		
	public void setPreviousPool() {
			
		for (int i = 0; i < poolNameTable.length; i++) {

			if (poolNameTable [i] == pool) {
				pool =  poolNameTable [(i - 1 + poolNameTable.length) % poolNameTable.length];
				i = poolNameTable.length;
			}
		}
		
		setChanged ();
		notifyObservers ();
	}
	
	public void setPool (char parPool) {
		
		pool = parPool;
		setChanged ();
		notifyObservers ();
	}
	
	// Convert the pool into a int
	public static int poolToInt (char parPool) {
		
		for (int i = 0; i < poolNameTable.length; i++) {

			if (poolNameTable [i] == parPool)
				return i;
		}
		
		return 0;
	}
}
