package view ;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel ;

import panel.Calendar;
import panel.Match;
import panel.Team;
import panel.TeamEndOfPool;

public class MainPanel extends JPanel {
	
	// Panel initialization
	Calendar calendar = new Calendar () ;
	JPanel calendarPanel;
	Team team = new Team ();
	Match match = new Match ();
	AllTeamPanel allTeamPanel = new AllTeamPanel (team.getTeamModel(), calendar.getScoreModel());
	JLabel logo;
	TeamEndOfPool teamEndOffPool = new TeamEndOfPool (calendar.getScoreModel());
	
	// ActionListeners
	
	public MainPanel(){
		
		// background color
		//setBackground(new Color(0,160,0));

		// logo
		logo = new JLabel(new ImageIcon("images/world_cup_logo.jpg"));
		
		// add
		add (logo);
		add (calendar.getCalendarView ());
		add (calendar.getAddingPanel());
		//calendar.getAddingPanel().setVisible(false);
		add (team.getTeamView());
		team.getTeamView().setVisible(false);
		add (match.getMatchView());
		match.getMatchView().setVisible(false);
		add (allTeamPanel);
		allTeamPanel.setVisible(false);
		add (teamEndOffPool.getTeamView());
		teamEndOffPool.getTeamView().setVisible(false);
	}
	
	public void setAddingPanelVisible () {
		
		calendar.getAddingPanel().setVisible(true);
		team.getTeamView().setVisible(false);
		teamEndOffPool.getTeamView().setVisible(false);
		match.getMatchView().setVisible(false);
		allTeamPanel.setVisible(false);
	}
	
	public void setTeamAViewVisible () {
		
		//calendar.getAddingPanel().setVisible(false);
		teamEndOffPool.getTeamView().setVisible(false);
		match.getMatchView().setVisible(false);
		allTeamPanel.setVisible(false);
		team.getTeamView().setVisible(true);
		team.getTeamView().showPanelA();
	}
	
	public void setTeamBViewVisible () {
		
		//calendar.getAddingPanel().setVisible(false);
		teamEndOffPool.getTeamView().setVisible(false);
		match.getMatchView().setVisible(false);
		allTeamPanel.setVisible(false);
		team.getTeamView().setVisible(true);
		team.getTeamView().showPanelB();
	}
	
	public void setTeamCViewVisible () {
		
		//calendar.getAddingPanel().setVisible(false);
		teamEndOffPool.getTeamView().setVisible(false);
		match.getMatchView().setVisible(false);
		allTeamPanel.setVisible(false);
		team.getTeamView().setVisible(true);
		team.getTeamView().showPanelC();
	}
	
	public void setTeamDViewVisible () {
		
		//calendar.getAddingPanel().setVisible(false);
		teamEndOffPool.getTeamView().setVisible(false);
		match.getMatchView().setVisible(false);
		allTeamPanel.setVisible(false);
		team.getTeamView().setVisible(true);
		team.getTeamView().showPanelD();
	}
	
	public void setTeamEViewVisible () {
		
		//calendar.getAddingPanel().setVisible(false);
		teamEndOffPool.getTeamView().setVisible(false);
		match.getMatchView().setVisible(false);
		allTeamPanel.setVisible(false);
		team.getTeamView().setVisible(true);
		team.getTeamView().showPanelE();
	}
	
	public void setTeamFViewVisible () {
		
		//calendar.getAddingPanel().setVisible(false);
		teamEndOffPool.getTeamView().setVisible(false);
		match.getMatchView().setVisible(false);
		allTeamPanel.setVisible(false);
		team.getTeamView().setVisible(true);
		team.getTeamView().showPanelF();
	}
	
	public void setTeamGViewVisible () {
		
		//calendar.getAddingPanel().setVisible(false);
		teamEndOffPool.getTeamView().setVisible(false);
		match.getMatchView().setVisible(false);
		allTeamPanel.setVisible(false);
		team.getTeamView().setVisible(true);
		team.getTeamView().showPanelG();
	}
	
	public void setTeamHViewVisible () {
		
		//calendar.getAddingPanel().setVisible(false);
		teamEndOffPool.getTeamView().setVisible(false);
		match.getMatchView().setVisible(false);
		allTeamPanel.setVisible(false);
		team.getTeamView().setVisible(true);
		team.getTeamView().showPanelH();
	}

	public void setMatchViewVisible () {
		
		//calendar.getAddingPanel().setVisible(false);
		match.getMatchView().setVisible(true);
		teamEndOffPool.getTeamView().setVisible(false);
		allTeamPanel.setVisible(false);
		team.getTeamView().setVisible(false);
	}
	
	public void setAllTeamVisible () {
		
		//calendar.getAddingPanel().setVisible(false);
		teamEndOffPool.getTeamView().setVisible(false);
		match.getMatchView().setVisible(false);
		allTeamPanel.setVisible(true);
		team.getTeamView().setVisible(false);
	}
	
	public void setTeamEndOffPoolVisible () {
		
		//calendar.getAddingPanel().setVisible(false);
		match.getMatchView().setVisible(false);
		teamEndOffPool.getTeamView().setVisible(true);
		allTeamPanel.setVisible(false);
		team.getTeamView().setVisible(false);
	}
}

