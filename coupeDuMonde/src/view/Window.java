package view ;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame ;
import javax.swing.JMenu ;
import javax.swing.JMenuBar ;
import javax.swing.JMenuItem;

public class Window extends JFrame implements ActionListener{
	
	// Menu creation
	private JMenuBar menuBar = new JMenuBar() ;
	private JMenu teamMenu = new JMenu ("Equipes") ;
	private JMenuItem allTeam = new JMenuItem ("Afficher toutes les �quipes");
	
	private MainPanel mainPanel;
	
	private JMenu poolMenu = new JMenu ("Groupes") ;
	private JMenuItem poolA = new JMenuItem ("Groupe A");
	private JMenuItem poolB = new JMenuItem ("Groupe B");
	private JMenuItem poolC = new JMenuItem ("Groupe C");
	private JMenuItem poolD = new JMenuItem ("Groupe D");
	private JMenuItem poolE = new JMenuItem ("Groupe E");
	private JMenuItem poolF = new JMenuItem ("Groupe F");
	private JMenuItem poolG = new JMenuItem ("Groupe G");
	private JMenuItem poolH = new JMenuItem ("Groupe H");
	
	private JMenu roundMenu = new JMenu ("1er tour") ;
	private JMenuItem firstRound = new JMenuItem ("Afficher les matchs du premier tour");
	
	private JMenu finalMenu = new JMenu ("Tableau Final") ;
	private JMenuItem teamsPoints = new JMenuItem ("Afficher les points des �quipes pendant le 1er tour");
	
	
	
	public Window(){
		// Title
		setTitle("Coupe du Monde de Football 2014") ;
			
		// Size
		setSize(900,1000);
			
		// Closure
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE) ;
			
		// ContentPane
		mainPanel = new MainPanel();
		setContentPane(mainPanel) ;
			
		// Visibility
		setVisible(true) ;
			
		// poolMenu
		poolMenu.add(poolA);
		poolA.addActionListener(this);
		poolMenu.add(poolB);
		poolB.addActionListener(this);
		poolMenu.add(poolC);
		poolC.addActionListener(this);
		poolMenu.add(poolD);
		poolD.addActionListener(this);
		poolMenu.add(poolE);
		poolE.addActionListener(this);
		poolMenu.add(poolF);
		poolF.addActionListener(this);
		poolMenu.add(poolG);
		poolG.addActionListener(this);
		poolMenu.add(poolH);
		poolH.addActionListener(this);
		
		teamMenu.add(allTeam);
		allTeam.addActionListener(this);
		menuBar.add(teamMenu) ;
		menuBar.add(poolMenu) ;
		roundMenu.add(firstRound);
		firstRound.addActionListener(this);
		menuBar.add(roundMenu) ;
		roundMenu.addActionListener(this);
		finalMenu.add(teamsPoints);
		teamsPoints.addActionListener(this);
		menuBar.add(finalMenu) ;
		

		setJMenuBar(menuBar);		
	}

	/**
	 * main function 
	 */
	public static void main(String[] args) {
		
		new Window() ;
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == poolA) 
			mainPanel.setTeamAViewVisible();
		
		else if (e.getSource() == poolB) 
			mainPanel.setTeamBViewVisible();
		
		else if (e.getSource() == poolC) 
			mainPanel.setTeamCViewVisible();
		
		else if (e.getSource() == poolD) 
			mainPanel.setTeamDViewVisible();
		
		else if (e.getSource() == poolE) 
			mainPanel.setTeamEViewVisible();
		
		else if (e.getSource() == poolF) 
			mainPanel.setTeamFViewVisible();
		
		else if (e.getSource() == poolG) 
			mainPanel.setTeamGViewVisible();
		
		else if (e.getSource() == poolH) 
			mainPanel.setTeamHViewVisible();
		
		else if (e.getSource() == allTeam) 
			mainPanel.setAllTeamVisible();
		
		else if (e.getSource() == firstRound) 
			mainPanel.setMatchViewVisible();
		
		else if (e.getSource() == teamsPoints) 
			mainPanel.setTeamEndOffPoolVisible();
	}

}

