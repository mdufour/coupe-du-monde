package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import model.ScoreModel;
import model.TeamModel;
import data.TeamData;

public class AllTeamPanel extends JPanel {

	// Variables
	private final int NUMBEROFTEAMS = 32;
		
	// The models
	TeamModel model;
	ScoreModel scoreModel;
	
	// The title of the panel
	private JLabel title;
		
	// A label for the name of the team
	private JLabel teamName;
	
	// A label for the flag of the team
	private JLabel teamFlag;		
		
	// A label to show the pool of the team
	private JLabel teamPool;
	
	// A label to show the score of the team
	private JLabel teamScore;
	private int score;
	
	// A team for the iterator
	private TeamData team;
		
	// GridLayout for the AllTeamPanel
	private GridLayout layout = new GridLayout(NUMBEROFTEAMS, 3, 5, 5);
	
	// A JPanel for the JScrollPane
	private JPanel panel = new JPanel ();
	
	// A Layout for the mainPanel
	private BorderLayout mainLayout = new BorderLayout (5, 5);
	
	// Constructor
	public AllTeamPanel (TeamModel parModel, ScoreModel parScoreModel)	{
			
		// Model
		model = parModel;
		scoreModel = parScoreModel;
		
		// Attribution of BorderLayout to the main panel (AllTeamPanel)
		setLayout(mainLayout);
		
		// Attribution of CardLayout to the center panel
		panel.setLayout(layout);
		setBorder(BorderFactory.createLineBorder(Color.black));
		
		// Creation of the table
		Iterator <TeamData> teamsIterator = model.getTeams().iterator();
		
		while (teamsIterator.hasNext()) {
			
			for (int i = 0; i < NUMBEROFTEAMS; i ++) {
					
				team = teamsIterator.next ();
				teamName = new JLabel (team.getCountry());
				teamFlag = new JLabel (new ImageIcon("images/" + team.getFlag() + ".svg.png"));
				teamPool = new JLabel ("Groupe " + team.getPool());
				
				HashMap <String, Integer> results = scoreModel.getResults();				
				
				if (results.get(team.getCountry()) == null)
					score = 0;
				
				else
					score = results.get(team.getCountry());
					
				teamScore = new JLabel (score + " points");
				
				panel.add(teamName);
				panel.add(teamFlag);
				panel.add(teamPool);
				//panel.add(teamScore);
			}			
		}
		
		// centerPanel
		JScrollPane scrollPane = new JScrollPane (panel);
		scrollPane.setPreferredSize(new Dimension (400, 500));
		
		// northPanel
		Font font = new Font("Arial",Font.BOLD, 17);
		title = new JLabel ("LISTE DES EQUIPES");
		title.setFont (font);
		title.setHorizontalAlignment(JLabel.CENTER);
				
		add ("Center", scrollPane);
		add ("North", title);
	}
}
