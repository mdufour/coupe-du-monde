package view;

import javax.swing.*;

import data.TeamData;
import model.TeamModel;

import java.awt.*;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;

public class TeamPanelSorted extends JPanel implements Observer {
	
	// Variables
	private final int NUMBEROFPANELS = 8;
	private final String STARTPOOL = "Groupe A";
	
	// The model
	TeamModel model;
	
	// A team
	TeamData team;
	
	// Center panel : contains the teams
	private JPanel centerPanel = new JPanel ();	
	
	// South panel : contains the buttons next and previous
	private JPanel southPanel = new JPanel ();
	
	// A table that contains the panels of the teams
	private JPanel [] teamsPanel = new JPanel [NUMBEROFPANELS];
	
	// Next and previous buttons
	private JButton next = new JButton ("Suivant");	
	private JButton previous = new JButton ("Precedent");	
		
	// A label to show the pool of the team
	private JLabel teamPool;
		
	// A label to show the name of the team
	private JLabel teamName;
	
	// A label to show the flag of the team
	private JLabel teamFlag;
	
	// The group of the team
	private char teamGroup;
	private int teamPoolIndex;
		
	// CardLayout for the panels
	private CardLayout panelsLayout = new CardLayout(5, 5);
	
	// GridLayout for the teams
	private GridLayout teamsLayout = new GridLayout(2,4, 5, 5);
	
	// FlowLayout for the southPanel
	private FlowLayout southLayout = new FlowLayout();	
	
	// BorderLayout for the teamPanel
	private BorderLayout principalLayout = new BorderLayout(5, 5);
	// End variables
	
	// get
	public JButton getPrevious ()
	{
		return previous;
	}
	
	public JButton getNext ()
	{
		return next;
	}
	
	public String getTeamName ()
	{
		return teamName.getText();
	}
	
	// set	
	public void setPanelsLayoutNext ()
	{
		panelsLayout.next(centerPanel);
	}
	
	public void setPanelsLayoutPrevious ()
	{
		panelsLayout.previous(centerPanel);
	}
	
	// Constructor
	public TeamPanelSorted (TeamModel parModel)	{
		
		// Model
		model = parModel;
		model.addObserver(this);
		
		// Attribution of BorderLayout to the principal panel (TeamPanel)		
		setLayout(principalLayout);		
		
		// Attribution of CardLayout to the centerPanel
		centerPanel.setLayout(panelsLayout);
		
		// Initialization of the table of JPanel		
		for (int i = 0; i < NUMBEROFPANELS; i ++)
		{
			teamsPanel [i] = new JPanel ();
			teamsPanel [i].setLayout(teamsLayout);
			centerPanel.add (teamsPanel [i], i);
		}
		
		Iterator<TeamData> iterator = model.getTeams().iterator();
		
		while (iterator.hasNext ()) {
			
			for (int j = 0; j < 4; j ++) {
				
				team = iterator.next ();
				teamName = new JLabel (team.getCountry ());
				String imagePath = "images/" + team.getFlag () + ".svg.png";
				teamFlag = new JLabel (new ImageIcon (imagePath));
				teamGroup = team.getPool ();
				
				teamPoolIndex = TeamModel.poolToInt(teamGroup);
				
				// add
				teamsPanel [teamPoolIndex].add (teamFlag);
				teamsPanel [teamPoolIndex].add (teamName);
			}
		}	
		
		// southPanel	
		southPanel.setLayout(southLayout);
		
		teamPool = new JLabel (STARTPOOL);
		
		southPanel.add(previous);
		southPanel.add(teamPool);
		southPanel.add(next);
	
		// CenterPanel
		panelsLayout.show (centerPanel, "0");	
		
		// add the different element (southPanel + centerPanel)
		add("Center", centerPanel); 
		add("South", southPanel);	
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		
		teamPool.setText(model.getPoolString());		
	}
}
