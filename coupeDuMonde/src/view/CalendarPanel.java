package view;

import model.CalendarModel;
import model.Date;
import javax.swing.*;

import panel.Calendar;
import exception.DateException;

import java.awt.*;
import java.util.Observable;
import java.util.Observer;

public class CalendarPanel extends JPanel implements Observer {
	
	// Variables
	private final int STARTMONTH = 6;
	private final int NUMBEROFMONTHS = 36;
	private int month = 1;
	private int year;
	private CalendarModel model;
	
	// Centre panel : contains the calendar
	private JPanel centerPanel = new JPanel ();	
	
	// South panel : contains the buttons next and previous
	private JPanel southPanel = new JPanel ();
	
	// A table that contains 2 panels : one for May and the other for June.
	private JPanel [] monthsPanel = new JPanel [NUMBEROFMONTHS];
	
	// Next and previous buttons
	private JButton next = new JButton ("Suivant");	
	private JButton previous = new JButton ("Precedent");	
			
	// A label to show the name of the month
	private JLabel monthName = new JLabel ();
	
	// The title of the panel
	private JLabel title;	
	
	// The logo of the panel
	private JLabel logo;
	
	// CardLayout for the months
	private CardLayout monthsLayout = new CardLayout(5, 5);
	
	// GridLayout for the days
	private GridLayout daysLayout = new GridLayout(0,7, 5, 5);
	
	// FlowLayout for the southPanel
	private FlowLayout southLayout = new FlowLayout();	
	
	// BorderLayout for the calendarPanel
	private BorderLayout principalLayout = new BorderLayout(5, 5);
	
	// JButton for the day
	private DayJButton day;
	
	private Date dayDate; 
	// End variables
	
	// get
	public JButton getPrevious ()
	{
		return previous;
	}
	
	public JButton getNext ()
	{
		return next;
	}
	
	public String getMonthName ()
	{
		return monthName.getText();
	}
	
	// set
	public void setTextMonthName (String text)
	{
		monthName.setText(text);
	}
	
	public void setMonthsLayoutNext ()
	{
		monthsLayout.next(centerPanel);
	}
	
	public void setMonthsLayoutPrevious ()
	{
		monthsLayout.previous(centerPanel);
	}
	
	// Constructor
	public CalendarPanel (CalendarModel parModel)	{
		
		
		// Initialization of the model
		model = parModel;		
		model.addObserver(this);
		
		// Attribution of BorderLayout to the principal panel (CalendarPanel)		
		setLayout(principalLayout);		
		setBorder(BorderFactory.createLineBorder(Color.black));
		
		// Attribution of CardLayout to the centerPanel
		centerPanel.setLayout(monthsLayout);
		
		// dayDate and year definition
		try {
			dayDate = new Date (1, STARTMONTH, new Date().getYear());
		} 
		catch (DateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		year = dayDate.getYear();
		
		
		// Initialization of the table of JPanel		
		for (int i = 0; i < NUMBEROFMONTHS; i ++)
		{
			monthsPanel [i] = new JPanel ();
			monthsPanel [i].setLayout(daysLayout);
			
			month = (i+STARTMONTH)%12 ;
			if (month == 0) {
				month = 12;
			}
			
			else if (month == 1) {
				year ++;
			}			
			
			for (int j = 0; j < Date.nbDays (month, year); j ++)
			{	
				day = new DayJButton (dayDate);				
				monthsPanel [i].add (day);		
				Calendar.addActionListener (day);
				dayDate = dayDate.next();
			}
			
			// For February
			if ((i + STARTMONTH)%12 == 2)
			{
				monthsPanel [i].add(day = new DayJButton (dayDate));
				day.setVisible(false);
			}
			
			centerPanel.add (monthsPanel [i], Date.monthString((i + STARTMONTH)%12) + year);
		}	
		
		// southPanel	
		southPanel.setLayout(southLayout);
		
		southPanel.add(previous);
		southPanel.add(monthName);
		southPanel.add(next);
		
		monthName.setText(Date.monthString(STARTMONTH));			
	
		// CenterPanel
		monthsLayout.show (centerPanel, Date.monthString(STARTMONTH) + 2014);	
		
		// northPanel
		Font font = new Font("Arial",Font.BOLD, 17);
		title = new JLabel ("CALENDRIER");
		title.setFont (font);
		title.setHorizontalAlignment(JLabel.CENTER);
				
		// add the different element (southPanel + centerPanel)
		add("Center", centerPanel); 
		add("South", southPanel);
		add("North", title);
	}

	public void update(Observable o, Object arg) {
		
		monthName.setText(model.getMonthName());		
	}
}
