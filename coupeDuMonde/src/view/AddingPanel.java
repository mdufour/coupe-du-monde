package view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Observable;
import java.util.Observer;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField ;

import data.StadiumData;
import data.TeamData;
import model.CalendarModel;
import model.Date;

public class AddingPanel extends JPanel implements Observer {

	// Model
	CalendarModel model;
	
	// Layout
	GridBagLayout layout = new GridBagLayout ();
	GridBagConstraints constraints = new GridBagConstraints ();
		
	// addButton and dateDisplay
	private JLabel dateDisplay;
	private Date date;
	private JButton addButton = new JButton("+") ;

	// Stadium selection (drop-down menu)
	private JLabel stadium = new JLabel("Stade") ;
	private JComboBox <String> stadiums;

	// Pool selection (RadioButton)
	private JLabel pool = new JLabel("Groupe") ;
	private ButtonGroup poolGroup = new ButtonGroup () ;
	private JRadioButton poolA = new JRadioButton("A") ;
	private JRadioButton poolB = new JRadioButton("B") ;
	private JRadioButton poolC = new JRadioButton("C") ;
	private JRadioButton poolD = new JRadioButton("D") ;
	private JRadioButton poolE = new JRadioButton("E") ;
	private JRadioButton poolF = new JRadioButton("F") ;
	private JRadioButton poolG = new JRadioButton("G") ;
	private JRadioButton poolH = new JRadioButton("H") ;
	
	// Teams selection (drop-down menu) 
	private JLabel team = new JLabel ("Equipes");
	private JComboBox <String> team1;
	private JComboBox <String> team2;
	
	// Scores
	private JLabel score = new JLabel ("Scores");
	private JTextField score1 = new JTextField() ;
	private JTextField score2 = new JTextField() ;
	
	// get
	public JButton getAddButton () {
		
		return addButton;
	}
	
	public String getScore1 () {
		
		return score1.getText();
	}

	public String getScore2 () {
	
		return score2.getText();
	}
	
	public String getTeam1 () {
		
		return (String) team1.getSelectedItem ();
	}
	
	public String getTeam2 () {
		
		return (String) team2.getSelectedItem ();
	}
	
	public String getPool () {
		
		return ((AbstractButton) poolGroup.getSelection()).getText();
	}
	
	public Date getDate() {
		
		return date;
	}
	
	// Constructor
	public AddingPanel (CalendarModel parModel) {
		
		// Layout and border of the panel
		setLayout (layout);
		setBorder(BorderFactory.createLineBorder(Color.black));
		
		// Model
		model = parModel;
		model.addObserver(this);
		
		// JComboBox
		stadiums = new JComboBox <String> (StadiumData.stadiums("data/stadium_2014.xml"));		
		team1 = new JComboBox <String> (TeamData.teams("data/equipes_2014.xml"));
		team2 = new JComboBox <String> (TeamData.teams("data/equipes_2014.xml"));
		
		// ButtonGroup
		poolGroup.add(poolA);
		poolGroup.add(poolB);
		poolGroup.add(poolC);
		poolGroup.add(poolD);
		poolGroup.add(poolE);
		poolGroup.add(poolF);
		poolGroup.add(poolG);
		poolGroup.add(poolH);
		
		// dateDisplay
		dateDisplay = new JLabel (new Date ().toString (3));
		date = new Date();
		
		// GridBagLayout
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.gridwidth = 3;		
		constraints.insets = new Insets (4, 4, 4, 4);
		add(dateDisplay, constraints);
		
		constraints.fill = GridBagConstraints.HORIZONTAL;	
		constraints.gridx = 3;
		constraints.gridy = 0;
		constraints.gridwidth = 2;
		constraints.insets = new Insets (4, 4, 4, 4);
		add(addButton, constraints);
		
		constraints.fill = GridBagConstraints.HORIZONTAL;	
		constraints.gridx = 0;
		constraints.gridy = 1;
		constraints.gridwidth = 1;
		constraints.insets = new Insets (4, 4, 4, 4);
		add(stadium, constraints);
		
		constraints.fill = GridBagConstraints.HORIZONTAL;	
		constraints.gridx = 1;
		constraints.gridy = 1;
		constraints.gridwidth = 4;		
		constraints.insets = new Insets (4, 4, 4, 4);
		add(stadiums, constraints);
		
		constraints.fill = GridBagConstraints.BOTH;	
		constraints.gridx = 0;
		constraints.gridy = 2;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.insets = new Insets (4, 4, 4, 4);
		add(pool, constraints);
		
		constraints.fill = GridBagConstraints.HORIZONTAL;	
		constraints.gridx = 1;
		constraints.gridy = 2;
		constraints.gridwidth = 1;
		constraints.insets = new Insets (4, 4, 4, 4);
		add(poolA, constraints);
		
		constraints.fill = GridBagConstraints.HORIZONTAL;	
		constraints.gridx = 2;
		constraints.gridy = 2;
		constraints.gridwidth = 1;
		add(poolB, constraints);
		
		constraints.fill = GridBagConstraints.HORIZONTAL;	
		constraints.gridx = 3;
		constraints.gridy = 2;
		constraints.gridwidth = 1;
		add(poolC, constraints);
		
		constraints.fill = GridBagConstraints.HORIZONTAL;	
		constraints.gridx = 4;
		constraints.gridy = 2;
		constraints.gridwidth = 1;
		add(poolD, constraints);
		
		constraints.fill = GridBagConstraints.HORIZONTAL;	
		constraints.gridx = 1;
		constraints.gridy = 3;
		constraints.gridwidth = 1;
		add(poolE, constraints);
		
		constraints.fill = GridBagConstraints.HORIZONTAL;	
		constraints.gridx = 2;
		constraints.gridy = 3;
		constraints.gridwidth = 1;
		add(poolF, constraints);
		
		constraints.fill = GridBagConstraints.HORIZONTAL;	
		constraints.gridx = 3;
		constraints.gridy = 3;
		constraints.gridwidth = 1;
		constraints.insets = new Insets (4, 4, 4, 4);
		add(poolG, constraints);
		
		constraints.fill = GridBagConstraints.HORIZONTAL;	
		constraints.gridx = 4;
		constraints.gridy = 3;
		constraints.gridwidth = 1;
		constraints.insets = new Insets (4, 4, 4, 4);
		add(poolH, constraints);
		
		constraints.fill = GridBagConstraints.HORIZONTAL;	
		constraints.gridx = 0;
		constraints.gridy = 4;
		constraints.gridwidth = 3;
		constraints.insets = new Insets (4, 4, 4, 4);
		add(team, constraints);
		
		constraints.fill = GridBagConstraints.HORIZONTAL;	
		constraints.gridx = 3;
		constraints.gridy = 4;
		constraints.gridwidth = 2;
		constraints.insets = new Insets (4, 4, 4, 4);
		add(score, constraints);
		
		constraints.fill = GridBagConstraints.HORIZONTAL;	
		constraints.gridx = 0;
		constraints.gridy = 5;
		constraints.gridwidth = 3;
		constraints.insets = new Insets (4, 4, 4, 4);
		add(team1, constraints);
		
		constraints.fill = GridBagConstraints.HORIZONTAL;	
		constraints.gridx = 3;
		constraints.gridy = 5;
		constraints.gridwidth = 2;
		constraints.insets = new Insets (4, 4, 4, 4);
		add(score1, constraints);	
		
		constraints.fill = GridBagConstraints.HORIZONTAL;	
		constraints.gridx = 0;
		constraints.gridy = 6;
		constraints.gridwidth = 3;
		constraints.insets = new Insets (4, 4, 4, 4);
		add(team2, constraints);
		
		constraints.fill = GridBagConstraints.HORIZONTAL;	
		constraints.gridx = 3;
		constraints.gridy = 6;
		constraints.gridwidth = 2;
		constraints.insets = new Insets (4, 4, 4, 4);
		add(score2, constraints);		
	}

	public void update(Observable o, Object arg) {
		
		dateDisplay.setText(model.getDate ().toString (3));
		date = model.getDate();
	}

	public void refresh() {
		
		score1.setText("");
		score2.setText("");
		team1.setSelectedIndex(0);
		team2.setSelectedIndex(0);
		stadiums.setSelectedIndex(0);		
		poolGroup.clearSelection();
	}
}
