package view;

import java.awt.Color;
import javax.swing.JButton;

import model.Date;

public class DayJButton extends JButton {

	// Color
	Color orange = new Color (255, 127, 0);
	Color blue = new Color (134, 223, 211);
	Color grey = new Color (220, 220, 220);
	Color darkGrey = new Color (200, 200, 200);
	
	// Date
	Date date;
	
	public DayJButton (Date parDate) {
		
		super (Integer.toString(parDate.getDay()));
		date = parDate;
		
		if (date.isCurrentDate ())
			setBackground(orange);
		
		else if (Date.isWeekEnd (parDate.getDay(), parDate.getMonth(), parDate.getYear()))
			setBackground(blue);
		
		else 
			setBackground(grey);
	}

	public Date getDate() {
		
		return date;
	}
}
