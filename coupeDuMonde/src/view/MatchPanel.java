package view;

import javax.swing.*;

import data.MatchData;
import data.TeamData;
import model.MatchModel;
import model.TeamModel;

import java.awt.*;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;

public class MatchPanel extends JPanel implements Observer {
	
	// Variables
	private final int NUMBEROFPANELS = 8;
	private final int NUMBEROFMATCHESOFAPOOL = 6;
	private final String STARTPOOL = "Groupe A";
	
	// The model
	MatchModel model;
	
	// A match
	MatchData match;
	
	// Center panel : contains the matches
	private JPanel centerPanel = new JPanel ();	
	
	// South panel : contains the buttons next and previous
	private JPanel southPanel = new JPanel ();
	
	// A table that contains the panels of the matches
	private JPanel [] matchesPanel = new JPanel [NUMBEROFPANELS];
	
	// Next and previous buttons
	private JButton next = new JButton ("Groupe suivant");	
	private JButton previous = new JButton ("Groupe precedent");	
		
	// A label to show the pool of the teams
	private JLabel teamsPool;
	
	// Pool of the teams
	private char teamPool;
	private int teamPoolIndex;
	
	// The two teams
	private TeamData teamA;
	private TeamData teamB;
	
	// A label to show the name of the team
	private JLabel teamAName;
	private JLabel teamBName;
	
	// A label to show the flag of the team
	private JLabel teamAFlag;
	private JLabel teamBFlag;
	
	// A label to show the scores of the teams
	private JLabel scores;
	
	// The title of the panel
	private JLabel title;
	
	// The date of the match
	private JLabel dateDisplay;
		
	// CardLayout for the panels
	private CardLayout panelsLayout = new CardLayout(5, 5);
	
	// GridLayout for the matches
	private GridLayout matchesLayout = new GridLayout(6, 6, 5, 5);
	
	// FlowLayout for the southPanel
	private FlowLayout southLayout = new FlowLayout();	
	
	// BorderLayout for the matchPanel
	private BorderLayout principalLayout = new BorderLayout(5, 5);
	// End variables
	
	// get
	public JButton getPrevious ()
	{
		return previous;
	}
	
	public JButton getNext ()
	{
		return next;
	}
	
	// set	
	public void setPanelsLayoutNext ()
	{
		panelsLayout.next(centerPanel);
	}
	
	public void setPanelsLayoutPrevious ()
	{
		panelsLayout.previous(centerPanel);
	}
	
	// Constructor
	public MatchPanel (MatchModel parModel)	{
		
		// Model
		model = parModel;
		model.addObserver(this);
		
		// Attribution of BorderLayout to the principalPanel (MatchPanel)
		setLayout(principalLayout);
		setBorder(BorderFactory.createLineBorder(Color.black));		
		
		// Attribution of CardLayout to the centerPanel
		centerPanel.setLayout(panelsLayout);
		
		// Initialization of the table of JPanel		
		for (int i = 0; i < NUMBEROFPANELS; i ++)
		{
			matchesPanel [i] = new JPanel ();
			matchesPanel [i].setLayout(matchesLayout);
			centerPanel.add (matchesPanel [i], i);
		}
			
		Iterator <MatchData> matchesIterator = model.getMatches().iterator();
		 
		for (int i = 0; i < NUMBEROFPANELS; i ++) {
			
			for (int j = 0; j < NUMBEROFMATCHESOFAPOOL; j ++) {				
				
				match = matchesIterator.next ();
				dateDisplay = new JLabel (match.getDate());				
				
				// teamA
				teamA = model.findTeam(match.getTeamA());
				teamAName = new JLabel (match.getTeamA ());
				teamAFlag = new JLabel (new ImageIcon("images/" + teamA.getFlag () + ".svg.png"));
				
				// teamB
				teamB = model.findTeam(match.getTeamB ());
				teamBName = new JLabel (match.getTeamB ());
				teamBFlag = new JLabel (new ImageIcon("images/" + teamB.getFlag () + ".svg.png"));
				
				
				// scores
				scores = new JLabel (match.getScoreA() + " - " + match.getScoreB());
				scores.setHorizontalAlignment(JLabel.CENTER);
				
				// pool
				teamPool = teamA.getPool ();				
				teamPoolIndex = TeamModel.poolToInt(teamPool);
				
				// add
				matchesPanel [teamPoolIndex].add (dateDisplay);
				matchesPanel [teamPoolIndex].add (teamAName);
				matchesPanel [teamPoolIndex].add (teamAFlag);
				matchesPanel [teamPoolIndex].add (scores);
				matchesPanel [teamPoolIndex].add (teamBFlag);
				matchesPanel [teamPoolIndex].add (teamBName);				
			}
		}	
		
		// southPanel	
		southPanel.setLayout(southLayout);
		
		teamsPool = new JLabel (STARTPOOL);
		
		southPanel.add(previous);
		southPanel.add(teamsPool);
		southPanel.add(next);
	
		// centerPanel
		panelsLayout.show (centerPanel, "0");	
		
		// northPanel
		Font font = new Font("Arial",Font.BOLD, 17);
		title = new JLabel ("LISTE DES MATCHS PAR GROUPE");
		title.setFont (font);
		title.setHorizontalAlignment(JLabel.CENTER);
		
		// add the different element (southPanel + centerPanel)
		add("Center", centerPanel);
		add("North", title);
		add("South", southPanel);	
	}
	
	public void refresh () {
		
		for (int i = 0; i < NUMBEROFPANELS; i ++)
		{
			centerPanel.remove (matchesPanel[i]);
		}
		
		// Initialization of the table of JPanel		
		for (int i = 0; i < NUMBEROFPANELS; i ++)
		{
			matchesPanel [i] = new JPanel ();
			matchesPanel [i].setLayout(matchesLayout);
			centerPanel.add (matchesPanel [i], i);
		}
			
		Iterator <MatchData> matchesIterator = model.getMatches().iterator();
		 
		for (int i = 0; i < NUMBEROFPANELS; i ++) {
			
			for (int j = 0; j < NUMBEROFMATCHESOFAPOOL; j ++) {				
				
				match = matchesIterator.next ();
				dateDisplay = new JLabel (match.getDate());				
				
				// teamA
				teamA = model.findTeam(match.getTeamA());
				teamAName = new JLabel (match.getTeamA ());
				teamAFlag = new JLabel (new ImageIcon("images/" + teamA.getFlag () + ".svg.png"));
				
				// teamB
				teamB = model.findTeam(match.getTeamB ());
				teamBName = new JLabel (match.getTeamB ());
				teamBFlag = new JLabel (new ImageIcon("images/" + teamB.getFlag () + ".svg.png"));
				
				
				// scores
				scores = new JLabel (match.getScoreA() + " - " + match.getScoreB());
				scores.setHorizontalAlignment(JLabel.CENTER);
				
				// pool
				teamPool = teamA.getPool ();				
				teamPoolIndex = TeamModel.poolToInt(teamPool);
				
				// add
				matchesPanel [teamPoolIndex].add (dateDisplay);
				matchesPanel [teamPoolIndex].add (teamAName);
				matchesPanel [teamPoolIndex].add (teamAFlag);
				matchesPanel [teamPoolIndex].add (scores);
				matchesPanel [teamPoolIndex].add (teamBFlag);
				matchesPanel [teamPoolIndex].add (teamBName);				
			}
		}	
		
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		
		teamsPool.setText(model.getPoolString());
	}
}
