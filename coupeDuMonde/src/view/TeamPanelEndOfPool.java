package view;

import javax.swing.*;

import data.TeamData;
import model.ScoreModel;
import model.TeamModel;

import java.awt.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import java.util.TreeSet;

public class TeamPanelEndOfPool extends JPanel implements Observer {
	
	// Class for the TreeSet
	public class TeamScores implements Comparable <TeamScores>{
		
		public String name;
		public String flag;
		public char pool;
		public Integer points;
		
		public TeamScores(String parName, String parFlag, char parPool, Integer parPoints) {
			
			name = parName;
			flag = parFlag;
			pool = parPool;
			points = parPoints;
		}

		@Override
		public int compareTo(TeamScores arg0) {
			
			if (this.points.compareTo(arg0.points) == 0)
				return this.name.compareTo(arg0.name);
			else
				return this.points.compareTo(arg0.points);
		}		
	}
	
	// TreeSet Data
	TeamScores teamScores;
	
	// Variables
	private final int NUMBEROFPANELS = 8;
	private final int NUMBEROFTEAMSPERPOOL = 4;
	private final String STARTPOOL = "Groupe A";
	
	// The models
	TeamModel model;
	ScoreModel scoreModel;
	
	// The title of the panel
	private JLabel title;
	
	// A table to order the points
	private TreeSet<TeamScores> teamOrderByPoints = new TreeSet<TeamScores> ();

	// A team
	TeamData team;
	
	// Center panel : contains the teams
	private JPanel centerPanel = new JPanel ();	
	
	// South panel : contains the buttons next and previous
	private JPanel southPanel = new JPanel ();
	
	// A table that contains the panels of the teams
	private JPanel [] teamsPanel = new JPanel [NUMBEROFPANELS];
	
	// Next and previous buttons
	private JButton next = new JButton ("Groupe suivant");	
	private JButton previous = new JButton ("Groupe precedent");	
		
	// A label to show the pool of the team
	private JLabel teamPool;
		
	// A label to show the name of the team
	private JLabel teamName;
	
	// A label to show the flag of the team
	private JLabel teamFlag;
	
	// A label to show the number of points of the team
	private JLabel teamPoints;
	
	// The group of the team
	private char teamGroup;
	private int teamPoolIndex;
		
	// CardLayout for the panels
	private CardLayout panelsLayout = new CardLayout(5, 5);
	
	// GridLayout for the teams
	private GridLayout teamsLayout = new GridLayout(NUMBEROFTEAMSPERPOOL, 3, 5, 5);
	
	// FlowLayout for the southPanel
	private FlowLayout southLayout = new FlowLayout();	
	
	// BorderLayout for the main panel
	private BorderLayout principalLayout = new BorderLayout(5, 5);
	// End variables
	
	// get
	public JButton getPrevious ()
	{
		return previous;
	}
	
	public JButton getNext ()
	{
		return next;
	}
	
	public String getTeamName ()
	{
		return teamName.getText();
	}
	
	// set	
	public void setPanelsLayoutNext ()
	{
		panelsLayout.next(centerPanel);
	}
	
	public void setPanelsLayoutPrevious ()
	{
		panelsLayout.previous(centerPanel);
	}
	
	// Constructor
	public TeamPanelEndOfPool (TeamModel parModel, ScoreModel parScoreModel)	{
		
		// Model
		model = parModel;
		scoreModel = parScoreModel;
		model.addObserver(this);
		
		// Attribution of BorderLayout to the principal panel (TeamPanel)		
		setLayout(principalLayout);		
		setBorder(BorderFactory.createLineBorder(Color.black));
		
		// Attribution of CardLayout to the centerPanel
		centerPanel.setLayout(panelsLayout);
		
		// Initialization of the table of JPanel		
		for (int i = 0; i < NUMBEROFPANELS; i ++)
		{
			teamsPanel [i] = new JPanel ();
			teamsPanel [i].setLayout(teamsLayout);
			centerPanel.add (teamsPanel [i], i);
		}
		
		Iterator<TeamData> iterator = model.getTeams().iterator();
		HashMap<String, Integer> results = scoreModel.getResults();
		
		while (iterator.hasNext ()) {
			
			team = iterator.next ();				
			
			teamScores = new TeamScores (team.getCountry (), "images/" + team.getFlag () + ".svg.png", team.getPool(), results.get(team.getCountry ()));
			
			teamOrderByPoints.add(teamScores);
		
		}
		
		Iterator<TeamScores> teamOrderByPointsIterator = teamOrderByPoints.iterator();
		
		for (int i = 0; i < NUMBEROFTEAMSPERPOOL*NUMBEROFPANELS; i ++) {			
			
			teamScores = teamOrderByPointsIterator.next();
			
			teamName = new JLabel (teamScores.name);
			teamFlag = new JLabel (new ImageIcon (teamScores.flag));				
			teamPoints = new JLabel (teamScores.points + " points");
			
			teamGroup = teamScores.pool;				
			teamPoolIndex = TeamModel.poolToInt(teamGroup);
			
			// add
			teamsPanel [teamPoolIndex].add (teamFlag);
			teamsPanel [teamPoolIndex].add (teamName);
			teamsPanel [teamPoolIndex].add (teamPoints);
		}
		
		// southPanel	
		southPanel.setLayout(southLayout);
		
		teamPool = new JLabel (STARTPOOL);
		
		southPanel.add(previous);
		southPanel.add(teamPool);
		southPanel.add(next);
	
		// CenterPanel
		panelsLayout.show (centerPanel, "0");	
		
		// northPanel
		Font font = new Font("Arial",Font.BOLD, 17);
		title = new JLabel ("CLASSEMENT DES POINTS PAR GROUPE");
		title.setFont (font);
		title.setHorizontalAlignment(JLabel.CENTER);
				
		// add the different element (southPanel + centerPanel)
		add("Center", centerPanel); 
		add("South", southPanel);
		add("North", title);
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		
		teamPool.setText(model.getPoolString());		
	}
	
	public void refresh () {
		
		teamOrderByPoints = new TreeSet<TeamScores> ();
		
		for (int i = 0; i < NUMBEROFPANELS; i ++)
		{
			centerPanel.remove (teamsPanel[i]);
		}
		
		// Initialization of the table of JPanel		
		for (int i = 0; i < NUMBEROFPANELS; i ++)
		{
			teamsPanel [i] = new JPanel ();
			teamsPanel [i].setLayout(teamsLayout);
			centerPanel.add (teamsPanel [i], i);
		}
		
		Iterator<TeamData> iterator = model.getTeams().iterator();
		HashMap<String, Integer> results = scoreModel.getResults();
		
		while (iterator.hasNext ()) {
			
			team = iterator.next ();				
			
			teamScores = new TeamScores (team.getCountry (), "images/" + team.getFlag () + ".svg.png", team.getPool(), results.get(team.getCountry ()));
			
			teamOrderByPoints.add(teamScores);
		
		}
		
		Iterator<TeamScores> teamOrderByPointsIterator = teamOrderByPoints.iterator();
		
		for (int i = 0; i < NUMBEROFTEAMSPERPOOL*NUMBEROFPANELS; i ++) {			
			
			teamScores = teamOrderByPointsIterator.next();
			
			teamName = new JLabel (teamScores.name);
			teamFlag = new JLabel (new ImageIcon (teamScores.flag));				
			teamPoints = new JLabel (teamScores.points + " points");
			
			teamGroup = teamScores.pool;				
			teamPoolIndex = TeamModel.poolToInt(teamGroup);
			
			// add
			teamsPanel [teamPoolIndex].add (teamFlag);
			teamsPanel [teamPoolIndex].add (teamName);
			teamsPanel [teamPoolIndex].add (teamPoints);
		}
	}	
}
